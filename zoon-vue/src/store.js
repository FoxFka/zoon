import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    jsonData: [
      {
        id: 1,
        title: 'Google',
        state: 'purple',
        rating: 5, // рейтинг
        review: 4, // отзывы
        reply: 3, // неотвеченные
        update: 2 // обновления
      }, {
        id: 2,
        title: 'Yandex',
        state: 'gray',
        rating: 4,
        review: 3,
        reply: 2,
        update: 1
      }, {
        id: 3,
        title: 'Rambler',
        state: 'orange',
        rating: 3,
        review: 2,
        reply: 1,
        update: 0
      }, {
        id: 4,
        title: '2gis',
        state: 'gray',
        rating: 2,
        review: 1,
        reply: 0,
        update: 0
      }, {
        id: 5,
        title: 'Waze',
        state: 'gray',
        rating: 1,
        review: 0,
        reply: 0,
        update: 0
      }],
    states: ['purple', 'orange', 'gray']
  },
  mutations: {
    updateJsonData(state, newObj) {
      const index = Math.floor(Math.random() * state.jsonData.length);
      state.jsonData = state.jsonData.map((item, i) => {
        if (i === index) {
          Object.assign(item, newObj)
        }
        return item;
      });
    }
  },
  actions: {
    r({}, {min, max}) {
      return Math.floor(Math.random() * (max - min + 1) + min);
    },
    async updateObject({commit, dispatch}) {
      commit('updateJsonData', {
        state: this.getters.states[Math.floor(Math.random() * this.getters.states.length)],
        rating: await dispatch('r', {min: 1, max: 5}),
        review: await dispatch('r', {min: 0, max: 200}),
        reply: await dispatch('r', {min: 0, max: 20}),
        update: await dispatch('r', {min: 0, max: 5})
      })
    }
  },
  getters: {
    jsonData: s => s.jsonData,
    states: s => s.states
  }
})
