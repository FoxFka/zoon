'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const prefixer = require('gulp-autoprefixer');
const less = require('gulp-less');
const browserSync = require("browser-sync");
const reload = browserSync.reload;
const cleanCSS = require('gulp-clean-css');

const config = {
  server: {
    baseDir: "./dist"
  }
};

const lessPath = 'src/less/**/[^_]*.less';

gulp.task('build', function () {
  gulp.src(lessPath)
    .pipe(less())
    .pipe(prefixer({ browsers: ['last 5 versions'] }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/css'))
});

gulp.task('watch', function () {
  browserSync(config);
  watch('src/less/**/*.less', function (event, cb) {
    gulp.start('build');
    browserSync.reload();
  });
  watch('dist/index.html', function(event, cb) {
    browserSync.reload();
  });
});

gulp.task('default', ['build', 'watch']);